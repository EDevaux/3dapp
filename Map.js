//initialisation of the map center
var mymap = L.map('mapid', { center: [60.7961129, 11.0590417], zoom: 14.5});
//openstreetmap layer
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
  attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(mymap);


//first marker placed
var marker = L.marker([60.796, 11.059]).addTo(mymap);
//initialisation popup
var popup = L.popup();

function onMapClick(e) {
  //creation of a marker on click
  var marker = L.marker(e.latlng).addTo(mymap);
    //creation of a popup on same click
    popup
        .setLatLng(e.latlng) //recuperation of is localisation
        .setContent("You clicked the map at " + e.latlng.toString()) //text written
        .openOn(mymap);
    var latitude = e.latlng.lat;
    var longitude = e.latlng.lng;
    document.getElementById("lat").value=latitude; //insert latitude into the input latitude
    document.getElementById("lng").value=longitude; //insert longitude into the input longitude
}
mymap.on('click', onMapClick);
